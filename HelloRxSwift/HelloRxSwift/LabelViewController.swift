//
//  LabelViewController.swift
//  HelloRxSwift
//
//  Created by Derek on 2020/4/21.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LabelViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        demo2()
    }
    
    ///将数据绑定到 attributedText 属性上（富文本）
    func demo2() {
        let timer = Observable<Int>.interval(0.1, scheduler: MainScheduler.instance)
        
        timer.map(formatTimeInterval(number:))
            .bind(to: label.rx.attributedText)
        .disposed(by: disposeBag)
    }
    
    func formatTimeInterval(number: Int) -> NSMutableAttributedString {
        let str = "\(number)"
        let attributesStr = NSMutableAttributedString(string: str)
        attributesStr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSMakeRange(0, 1))
        
        return attributesStr
    }
    
    ///将数据绑定到 text 属性上（普通文本）
    func demo1() {
        let timer = Observable<Int>.interval(0.1, scheduler: MainScheduler.instance)
        
        timer.map({ "\($0)" })
          .bind(to: label.rx.text)
          .disposed(by: disposeBag)
    }
}
