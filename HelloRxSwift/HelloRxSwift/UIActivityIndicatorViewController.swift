//
//  UIActivityIndicatorViewController.swift
//  HelloRxSwift
//
//  Created by Derek on 2020/4/22.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class UIActivityIndicatorViewController: UIViewController {

    @IBOutlet weak var mySwitch: UISwitch!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        demo2()
    }
    
    ///UIApplication 网络请求指示器
    func demo2() {
        mySwitch.rx.isOn
            .bind(to: UIApplication.shared.rx.isNetworkActivityIndicatorVisible)
            .disposed(by: bag)
    }
    
    ///UIActivityIndicatorView（活动指示器）
    func demo1() {
        mySwitch.rx.isOn
            .bind(to: activityIndicator.rx.isAnimating)
            .disposed(by: bag)
    }

}
