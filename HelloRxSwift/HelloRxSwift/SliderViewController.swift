//
//  SliderViewController.swift
//  HelloRxSwift
//
//  Created by Derek on 2020/4/22.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SliderViewController: UIViewController {

    
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var stepper: UIStepper!
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        demo2()
        demo3()
    }
    
    ///使用滑块（stepper）来控制 slider 的步长
    func demo4() {
        stepper.rx.value.map({ Float($0) })
            .bind(to: slider.rx.value)
            .disposed(by: bag)
    }
    
    ///使用滑块（slider）来控制 stepper 的步长
    func demo3() {
        slider.rx.value.map({ Double($0) })
            .bind(to: stepper.rx.value)
            .disposed(by: bag)
    }
    
    ///UIStepper（步进器）
    func demo2() {
        stepper.rx.value
            .subscribe(onNext: { print("當前值為：\($0)")})
            .disposed(by: bag)
        
    }
    
    ///UISlider（滑块）
    func demo1() {
        slider.rx.value
            .subscribe(onNext: { print("當前值為：\($0)") })
            .disposed(by: bag)
    }
}
