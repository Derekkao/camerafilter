//
//  ButtonViewController.swift
//  HelloRxSwift
//
//  Created by Derek on 2020/4/22.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ButtonViewController: UIViewController {

    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var switch1: UISwitch!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        demo4()
    }
    
    ///按钮是否选中（isSelected）的绑定
    func demo5() {
        //默认选中第一个按钮
        button1.isSelected = true
        
        //强制解包，避免后面还需要处理可选类型
        let buttons = [button1, button2, button3].map({ $0! })
        
        //创建一个可观察序列，它可以发送最后一次点击的按钮（也就是我们需要选中的按钮）
        let selectedBtn = Observable.from(buttons.map({ (btn) in
            btn.rx.tap.map { btn }
        })).merge()
        
        //对于每一个按钮都对selectedButton进行订阅，根据它是否是当前选中的按钮绑定isSelected属性
        for btn in buttons {
//            selectedBtn.map({ $0 == btn })
//                .bind(to: btn.rx.isSelected)
//                .disposed(by: disposeBag)
            selectedBtn.map({ $0 == btn })
                .subscribe(onNext: {
                    btn.backgroundColor = $0 ? UIColor.red : UIColor.clear
                })
            .disposed(by: disposeBag)
        }
    }
    
    ///按钮是否可用（isEnabled）的绑定
    func demo4() {
        
        switch1.rx.isOn
            .subscribe(onNext: { print("當前開關狀態：\($0)") })
            .disposed(by: disposeBag)
        
        switch1.rx.isOn
            .bind(to: button.rx.isEnabled)
            .disposed(by: disposeBag)
    }
    
    ///按钮图标（image）的绑定
    func demo3() {
        let timer = Observable<Int>.interval(1, scheduler: MainScheduler.instance)
        timer.map({ (number) -> UIImage in
            let name = number % 2 == 0 ? "home-run" : "home-7"
            return UIImage(named: name) ?? UIImage()
        })
            .bind(to: button.rx.image())
            .disposed(by: disposeBag)
    }
    
    ///按钮标题（title）的绑定
    func demo2() {
        let timer = Observable<Int>.interval(1, scheduler: MainScheduler.instance)
        timer.map({ "計數：\($0)" })
            .bind(to: button.rx.title(for: .normal))
            .disposed(by: disposeBag)
    }
    
    ///按钮点击响应
    func demo1() {
        button.rx.tap
            .subscribe(onNext: { [weak self] in self?.showMessage("按鈕被點擊")})
            .disposed(by: disposeBag)
    }
}

extension ButtonViewController {
    func showMessage(_ text: String) {
        let alert = UIAlertController(title: text, message: nil, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "確定", style: .cancel, handler: nil)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
}
