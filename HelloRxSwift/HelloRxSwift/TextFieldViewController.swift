//
//  TextFieldViewController.swift
//  HelloRxSwift
//
//  Created by Derek on 2020/4/21.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TextFieldViewController: UIViewController {

    @IBOutlet weak var textFieldOne: UITextField!
    @IBOutlet weak var textFieldTwo: UITextField!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var textView: UITextView!
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        demo7()
    }
    
    func demo7() {
        let nameObservable = textFieldOne.rx.text.orEmpty.share()
        let passwordObservable = textFieldTwo.rx.text.orEmpty.share()
        
        Observable.combineLatest(nameObservable, passwordObservable) { (name, password) -> Bool in
            self.textFieldOne.backgroundColor = (name.count > 5) ? UIColor.clear : UIColor.lightGray
            self.textFieldTwo.backgroundColor = (name.count > 8) ? UIColor.clear : UIColor.lightGray
            self.button.backgroundColor = (name.count > 5 && password.count > 7) ? UIColor.red : UIColor.clear
            return name.count > 5 && password.count > 7
            }.bind(to: button.rx.isEnabled)
        .disposed(by: disposeBag)
    }
    
    ///同时监听多个 textField 内容的变化（textView 同理）
    func demo3() {
        
        Observable.combineLatest(textFieldOne.rx.text.orEmpty, textFieldTwo.rx.text.orEmpty) { (text1, text2) in
            return "您輸入的號碼是：\(text1)-\(text2)"
        }.map({ $0 })
            .bind(to: label.rx.text)
            .disposed(by: disposeBag)
    }
    
    ///将内容绑定到其他控件上
    func demo2() {
        
        //当文本框内容改变
        let input = textFieldOne.rx.text.orEmpty.asDriver().throttle(0.3)
       
        //内容绑定到另一个输入框中
        input.drive(textFieldTwo.rx.text).disposed(by: disposeBag)
        
        //内容绑定到文本标签中
        input.map({ "當前字數：\($0.count)" })
            .drive(label.rx.text)
            .disposed(by: disposeBag)
        
        //根据内容字数决定按钮是否可用
        input.map({ $0.count > 5 })
            .drive(button.rx.isEnabled)
            .disposed(by: disposeBag)
    }
    
    ///监听单个 textField 内容的变化（textView 同理）
    func demo1() {
        textFieldOne.rx.text.orEmpty.asObservable()
            .subscribe(onNext: { print("您輸入的是\($0)") })
            .disposed(by: disposeBag)
    }
}

extension TextFieldViewController {
    
    ///UITextView 独有的方法
    func demo6() {
        textView.rx.didBeginEditing
            .subscribe(onNext: { print("開始編輯") })
            .disposed(by: disposeBag)
        
        textView.rx.didEndEditing
            .subscribe(onNext: { print("結束編輯") })
            .disposed(by: disposeBag)
        
        textView.rx.didChange
            .subscribe(onNext: { print("內容發生改變") })
            .disposed(by: disposeBag)
        
        textView.rx.didChangeSelection
            .subscribe(onNext: { print("選中部分發生變化") })
            .disposed(by: disposeBag)
    }
    
    func demo5() {
        textFieldOne.rx.controlEvent(.editingDidEndOnExit)
            .asObservable()
            .subscribe(onNext: {
                print("結束編輯內容")
                self.textFieldTwo.becomeFirstResponder() })
            .disposed(by: disposeBag)
        
        textFieldTwo.rx.controlEvent(.editingDidEndOnExit)
            .asObservable().subscribe(onNext: {
                print("結束編輯內容")
                self.textFieldTwo.resignFirstResponder() })
            .disposed(by: disposeBag)
    }
    
    ///事件监听
    func demo4() {
        textFieldOne.rx.controlEvent(.editingDidBegin)
            .asObservable()
            .subscribe(onNext: { print("開始編輯內容") })
            .disposed(by: disposeBag)
    }
}
