//
//  SegmentedControlViewController.swift
//  HelloRxSwift
//
//  Created by Derek on 2020/4/22.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SegmentedControlViewController: UIViewController {
    
    
    @IBOutlet weak var segmented: UISegmentedControl!
    @IBOutlet weak var imageView: UIImageView!
    
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        demo2()
    }
    
    ///当 segmentedControl 选项改变时，imageView 会自动显示相应的图片。
    func demo2() {
        //创建一个当前需要显示的图片的可观察序列
        let showImageObservable = segmented.rx.selectedSegmentIndex.map { (index) -> UIImage in
            let images = ["home-7", "home-run", "kobe"]
            return UIImage(named: images[index]) ?? UIImage()
        }
        
        //把需要显示的图片绑定到 imageView 上
        showImageObservable.bind(to: imageView.rx.image).disposed(by: bag)
    }
    
    ///UISegmentedControl（分段选择控件）
    func demo1() {
        segmented.rx.selectedSegmentIndex
            .subscribe(onNext: { print("當前項：\($0)") })
        .disposed(by: bag)
    }
}
