//
//  ViewController.swift
//  HelloRxSwift
//
//  Created by Derek on 2020/3/13.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit
import RxSwift

enum CustomError: Error {
    case somethingError
}

class ViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.delegate = self
        demo15()
    }
    
    func demo16() {
        example("ignoreElements") {
            let task = PublishSubject<String>()
            let bossIsGone = PublishSubject<Void>()
            let bag = DisposeBag()
            
            task.takeUntil(bossIsGone).subscribe({ print($0) }).disposed(by: bag)
            
            task.onNext("T1")
            task.onNext("T2")
            task.onNext("T3")
            bossIsGone.onNext(())
            task.onNext("T4")
            task.onCompleted()
        }
    }
    
    func demo15() {
        example("ignoreElements") {
            let task = PublishSubject<String>()
            let bag = DisposeBag()
            
            //指定訂閱的事件
            //task.elementAt(2).subscribe({ print($0) }).disposed(by: bag)
            //task.filter({ $0 == "T2" }).subscribe({ print($0) }).disposed(by: bag)
            
            //指定訂閱的事件從哪開始
            //task.take(2).subscribe({ print($0) }).disposed(by: bag)
            task.takeWhile({ $0 != "T3" }).subscribe({ print($0) }).disposed(by: bag)
            
            task.onNext("T1")
            task.onNext("T2")
            task.onNext("T3")
            task.onNext("T4")
            task.onCompleted()
        }
    }
    
    func demo14() {
        example("ignoreElements") {
            let task = PublishSubject<String>()
            let bag = DisposeBag()
            
            // distinctUntilChanged 忽略重複出現的事件
            task.distinctUntilChanged().subscribe({ print($0) }).disposed(by: bag)
            
            task.onNext("T1")
            task.onNext("T2")
            task.onNext("T3")
            task.onNext("T3")
            task.onNext("T4")
            task.onCompleted()
        }
    }
    
    func demo13() {
        example("ignoreElements") {
            let task = PublishSubject<String>()
            let bossIsAngry = PublishSubject<Void>()
            let bag = DisposeBag()
            
            // 到 bossIsAngry 才會執行後續的事件
            task.skipUntil(bossIsAngry).subscribe({ print($0) }).disposed(by: bag)
            
            task.onNext("T1")
            task.onNext("T2")
            bossIsAngry.onNext(())
            task.onNext("T3")
            task.onCompleted()
        }
    }
    
    func demo12() {
        example("ignoreElements") {
            let task = PublishSubject<String>()
            let bag = DisposeBag()
            //忽略掉全部任務
            //task.ignoreElements().subscribe({ print($0) }).disposed(by: bag)
            //忽略掉（前兩個）指定任務
            //task.skip(2).subscribe({ print($0) }).disposed(by: bag)
            //忽略掉指定任務 第一次 T1 != T2,去掉 T1，第二次 T2 == T2，返回 false，就不再往下走了
            task.skipWhile({ $0 != "T2" }).subscribe({ print($0) }).disposed(by: bag)
            
            task.onNext("T1")
            task.onNext("T2")
            task.onNext("T3")
            task.onCompleted()
        }
    }
    
    func example(_ description: String, action: () -> ()) {
        print("=================\(description)==================")
        action()
    }
    
    func demo11() {
        let stringVariable = Variable("episode1")
        //let sub1 = stringVariable.asObservable().subscribe(onNext: { print("sub1: \($0)") })
        stringVariable.value = "Episode2"
        print(stringVariable.value)
    }
    
    ///可以指定訂閱先前的歷史事件範圍
    func demo10() {
        let subject = ReplaySubject<String>.create(bufferSize: 2)
        let sub1 = subject.subscribe(onNext: { print("Sub1 - what happened: \($0)") })
        subject.onNext("episode1 updated")
        subject.onNext("episode2 updated")
        subject.onNext("episode3 updated")
        sub1.dispose()
        //前面 subject 指定的事件範圍為2，所以 sub2 只會訂閱到前兩個歷史事件
        let sub2 = subject.subscribe(onNext: { print("Sub2 - what happened: \($0)") })
        sub2.dispose()
    }
    
    ///試閱制
    func demo9() {
        let subject = BehaviorSubject<String>(value: "RxSwift")
        // PublishSubject 是採取訂閱方式，所以要把訂閱放在通知 subject 前面，先有訂閱的人才會收到後面的消息
        let sub1 = subject.subscribe(onNext: { print("Sub1 - what happened: \($0)") })
        subject.onNext("episode1 updated")
        sub1.dispose()
        // sub2 會訂閱到最近一次的歷史事件
        let sub2 = subject.subscribe(onNext: { print("Sub2 - what happened: \($0)") })
        subject.onNext("episode2 updated")
        subject.onNext("episode3 updated")
        sub2.dispose()
    }
    
    ///會員制
    func demo8() {
        let subject = PublishSubject<String>()
        // PublishSubject 是採取訂閱方式，所以要把訂閱放在通知 subject 前面，先有訂閱的人才會收到後面的消息
        let sub1 = subject.subscribe(onNext: { print("Sub1 - what happened: \($0)") })
        subject.onNext("episode1 updated")
        sub1.dispose()
        let sub2 = subject.subscribe(onNext: { print("Sub2 - what happened: \($0)") })
        subject.onNext("episode2 updated")
        subject.onNext("episode3 updated")
        sub2.dispose()
    }
    
    func demo7() {
        let customOb = Observable<Int>.create { (observer) -> Disposable in
            observer.onNext(10)
            observer.onNext(11)
            observer.onError(CustomError.somethingError)
            observer.onCompleted()
            
            return Disposables.create()
        }
        
        let disposeBag = DisposeBag()
        customOb.debug().subscribe(onNext: { print($0) },
                           onCompleted: { print("completed") },
                           onDisposed: { print("game over") }
        ).disposed(by: disposeBag)
    }
    
    func demo6() {
        var bag = DisposeBag()
        Observable<Int>.interval(1, scheduler: MainScheduler.instance).subscribe(
            onNext: { print("subscribed:\($0)") },
            onError: { print($0) },
            onCompleted: nil,
            onDisposed: { print("The queue was disposed.") }).disposed(by: bag)
        delay(5) {
            bag = DisposeBag()
        }
        dispatchMain()
    }
    
    func delay(_ delay: Double, completed:@escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            completed()
        }
    }
    
    ///
    func demo5() {
        //Observable.of("1","2","3","4","5","6","7","8","9")
        var evenNumberObservable = Observable.from(["1","2","3","4","5","6","7","8","9"]).map { Int($0) }.filter {
            if let item = $0, item % 2 == 0 {
                print("even \(item)")
                return true
            }
            return false
        }
        
        evenNumberObservable.skip(2).subscribe(onNext: { (event) in
            print("Event: \(event)")
        }, onError: { (error) in
            print(error)
        }, onCompleted: {
            print("Completed")
        }) {
            print("disposed")
        }
    }
    
    func demo4() {
        let disposeBag = DisposeBag()
        
        let rxTemperature:Observable<Double> = Observable.of(20,23.5,27,29.5)
        rxTemperature.filter { (temp) -> Bool in
            temp > 27
        }.subscribe(onNext: { (temp) in
            print("高溫\(temp)度")
            }).disposed(by: disposeBag)
    }
    func demo3() {
        let disposeBag = DisposeBag()
        
        let subject = PublishSubject<String>()
        subject.onNext("Issue 1")
        subject.subscribe { (event) in
            print(event)
        }
    }
    
    func demo2() {
        let disposeBag = DisposeBag()
        Observable.of("a","b","c").subscribe {
            print($0)
        }.disposed(by: disposeBag)
        
        Observable<String>.create { (observer) -> Disposable in
            observer.onNext("A")
            observer.onCompleted()
            observer.onNext("?")
            return Disposables.create()
        }.subscribe(onNext: {
            print($0)
        }, onError: {
            print($0)
        }, onCompleted: {
            print("Completed")
        }) {
            print("Disposed")
        }.disposed(by: disposeBag)
    }

    func demo1() {
        let observable = Observable.just(1)
        let observable2 = Observable.of(1,2,3)
        let observable3 = Observable.of([1,2,3])
        let observable4 = Observable.from([1,2,3,4,5])
        
        observable.subscribe { (event) in
            if let element = event.element {
                print(element)
            }
        }
        
        let subscription3 = observable3.subscribe(onNext: { (event) in
            print(event)
        }, onError: nil, onCompleted: nil, onDisposed: nil)
        
        subscription3.dispose()
    }
}

//MARK: - UITextFieldDelegate
extension ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let n = Int(string) {
            if n % 2 == 0 {
                print("Even \(n)")
            }
        }
        return true
    }
}
