//
//  ViewController.swift
//  CameraFilter
//
//  Created by Derek on 2020/4/16.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit
import RxSwift

class ViewController: UIViewController {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var applyFilterButton: UIButton!
    
    let disposeBag = DisposeBag()
    
    @IBAction func applyFilterButtonPressed() {
        guard let sourceImage = self.photoImageView.image else { return }
        
        FilterService().applyFilterr(to: sourceImage).subscribe(onNext: { [weak self](filteredImage) in
            DispatchQueue.main.async {
                self?.photoImageView.image = filteredImage
            }
        }).disposed(by: disposeBag)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let naVC = segue.destination as? UINavigationController, let photosVC = naVC.viewControllers.first as? PhotosCollectionViewController else {
            print("segue.destination 錯誤")
            return
        }
        photosVC.selectedPhoto.subscribe(onNext: { [weak self](photo) in
            DispatchQueue.main.async {
                self?.updateUI(with: photo)
            }
            }).disposed(by: disposeBag)
    }
    
    private func updateUI(with image: UIImage) {
        self.photoImageView.image = image
        self.applyFilterButton.isHidden = false
    }

}

