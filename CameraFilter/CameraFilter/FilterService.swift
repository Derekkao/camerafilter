//
//  FilterService.swift
//  CameraFilter
//
//  Created by Derek on 2020/4/16.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit
import CoreImage
import RxSwift

class FilterService {
    private var context: CIContext
    
    init() {
        self.context = CIContext()
    }
    
    func applyFilterr(to inputImage: UIImage) -> Observable<UIImage> {
        return Observable<UIImage>.create { (observer) -> Disposable in
            self.applyFilter(to: inputImage) { (filterImage) in
                observer.onNext(filterImage)
            }
            return Disposables.create()
        }
    }
    
    func applyFilter(to inputImage: UIImage, completion:@escaping ((UIImage) -> Void)) {
        let filter = CIFilter(name: "CICMYKHalftone")
        filter?.setValue(5.0, forKey: kCIInputWidthKey)
        if let sourceImage = CIImage(image: inputImage) {
            filter?.setValue(sourceImage, forKey: kCIInputImageKey)
            if let cgImage = self.context.createCGImage(filter?.outputImage ?? CIImage(), from: filter?.outputImage?.extent ?? CGRect(x: 0, y: 0, width: 0, height: 0)) {
                let processedImage = UIImage(cgImage: cgImage, scale: inputImage.scale, orientation: inputImage.imageOrientation)
                completion(processedImage)
            }
        }
    }
}
