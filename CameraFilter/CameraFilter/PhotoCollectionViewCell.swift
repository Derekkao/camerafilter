//
//  PhotoCollectionViewCell.swift
//  CameraFilter
//
//  Created by Derek on 2020/4/16.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
}
