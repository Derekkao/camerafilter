//
//  TodoDetailViewController.swift
//  ToDoDemo
//
//  Created by Mars on 26/04/2017.
//  Copyright © 2017 Mars. All rights reserved.
//

import UIKit
import RxSwift

class TodoDetailViewController: UITableViewController {
    
    //fileprivate let todoSubject = ReplaySubject<TodoItem>.create(bufferSize: 0)
    fileprivate let todoSubject = PublishSubject<TodoItem>()
    //這樣外部只能通過 todo 訂閱，而內部可以通過 todoSubject 發送消息
    var todo: Observable<TodoItem> {
        return todoSubject.asObservable()
    }
    var todoItem: TodoItem!
    let bag = DisposeBag()
    
    @IBOutlet weak var todoName: UITextField!
    @IBOutlet weak var isFinished: UISwitch!
    @IBOutlet weak var doneBarBtn: UIBarButtonItem!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        todoName.becomeFirstResponder()
        if let todoItem = todoItem {
            todoName.text = todoItem.name
            isFinished.isOn = todoItem.isFinished
        } else {
            todoItem = TodoItem()
        }
    }
    
    @IBAction func cancel() {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func done() {
        
        todoItem.name = todoName.text!
        todoItem.isFinished = isFinished.isOn
        
        todoSubject.onNext(todoItem)
        // 加入 onCompleted，在 subscribe 的地方最後就不用寫 disposed 了
        todoSubject.onCompleted()
        
        dismiss(animated: true, completion: nil)
    }
}

extension TodoDetailViewController {
    override func tableView(_ tableView: UITableView,
                            willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return nil
    }
}

extension TodoDetailViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let oldText = textField.text! as NSString
        let newText = oldText.replacingCharacters(in: range, with: string) as NSString
        
        doneBarBtn.isEnabled = newText.length > 0
        
        return true
    }
}
