//
//  PHPhotoLibrary+Rx.swift
//  ToDoDemo
//
//  Created by Derek on 2020/4/20.
//  Copyright © 2020 Mars. All rights reserved.
//

import Foundation
import Photos
import RxSwift

extension PHPhotoLibrary {
    static var isAuthorized: Observable<Bool> {
        return Observable.create { (observer) in
            DispatchQueue.main.async {
                if authorizationStatus() == .authorized {
                    observer.onNext(true)
                    observer.onCompleted()
                } else {
                    requestAuthorization { (authorizationStatus) in
                        observer.onNext(authorizationStatus == .authorized)
                        observer.onCompleted()
                    }
                }
            }
            return Disposables.create()
        }
    }
}
